package zestaw1;

import java.util.Scanner;

public class Zadanie10 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int sum = 0;
        int read;
        do {
            read = scanner.nextInt();
            sum = sum + read;
        } while(read != 0);

        System.out.println("Sum: " + sum);
    }

}
