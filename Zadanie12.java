package zestaw1;

import java.util.Scanner;

public class Zadanie12 {

    public static void main(String[] args) {

        int random = (int)(Math.random() * 100.0 + 1); // <1 ; 100>

        // random() -> <0.0 ; 1.0)
        // * 100 -> <0.0 ; 100.0)
        // + 1 -> <1.0 ; 101.0)
        // (int) -> <1 ; 100>

        Scanner scanner = new Scanner(System.in);

        int read;
        do {
            read = scanner.nextInt();
            if (read < random) {
                System.out.println("Podałeś/aś za małą liczbę.");
            } else if (read > random) {
                System.out.println("Podałeś/aś za dużą liczbę.");
            } else {
                System.out.println("Gratulacje! Zgadłeś/aś!");
            }
        } while(read != random);

    }

}
